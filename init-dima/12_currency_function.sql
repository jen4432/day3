create function equipment.models_price_in_currency() returns 
table(EUR bigint, USD bigint, BYN bigint, RUB bigint, CNY bigint) 
as $$
begin
   return query select  equipment.models.id, equipment.models.price * currency.EUR_ex_rate as EUR, equipment.models.price * currency.USD_ex_rate as USD,
        equipment.models.price * BYN_ex_rate as BYN, equipment.models.price * RUB_ex_rate as RUB, equipment.models.price * CNY_ex_rate AS CNY
   from equipment.models inner join equipment.currency 
   on equipment.models.currency_id = equipment.currency.id; 
end;
$$ language plpgsql;