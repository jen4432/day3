create table equipment.currency(
    id             bigserial primary key,
    currency_name  text   not null,
    updatetime     date   not null,
    EUR_ex_rate    bigint not null,
    USD_ex_rate    bigint not null,
    BYN_ex_rate    bigint not null,
    RUB_ex_rate    bigint not null,
    CNY_ex_rate    bigint not null,
    divider        bigint not null
);