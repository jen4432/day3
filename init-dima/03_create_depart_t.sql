create table equipment.departments(
    id              bigserial  primary key,
    manager_id      bigint  not null,
    department_name text not null
);