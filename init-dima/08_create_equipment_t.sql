create table equipment.equipments(
    id               bigserial primary key,
    serial_number    bigint  not null,
    warehouse_id     bigint  not null references equipment.warehouse(id),
    in_usage         boolean default false,
    model_id         bigint  not null references equipment.models(id)
);

create index if not exists equipments_model_id_idx on equipment.equipments(model_id);
create index if not exists equipments_warehouse_id_idx on equipment.equipments(warehouse_id);