create function equipment.new_contract_fnc() returns trigger as $$
declare
mviews record;
begin
  for mviews in 
  select id as mv_id, in_usage as mv_usage from equipment.equipments where id = new.equipment_id
  loop
   if mviews.mv_usage = true then
   raise exception '% id of equipment now in usage', new.equipment_id;
   return null;
   end if;
  end loop;
  update equipment.equipments set in_usage = true where id = new.equipment_id;
  return new; 
end;
$$ language plpgsql;

create trigger new_contract before insert on equipment.employess_equipments for each row execute procedure equipment.new_contract_fnc();