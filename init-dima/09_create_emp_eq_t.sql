create table equipment.employess_equipments(
    employee_id  bigint references equipment.employes(id), 
    equipment_id bigint references equipment.equipments(id),
    start_date   date,
    end_date     date,
    primary key(employee_id, equipment_id)
);

create index if not exists employess_equipments_employee_id_idx  on equipment.employess_equipments(employee_id);
create index if not exists employess_equipments_equipment_id_idx on equipment.employess_equipments(equipment_id);