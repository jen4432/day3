create table equipment.warehouse(
    id                       bigserial primary key,
    warehouse_number         bigint not null,
    responsible_employee_id  bigint not null references equipment.employes(id)
);

create index if not exists warehouse_responsible_employee_id_idx on equipment.warehouse(responsible_employee_id);
