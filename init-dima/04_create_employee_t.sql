create table equipment.employes(
    id                 bigserial  primary key,
    department_id      bigint  not null references equipment.departments(id),
    manager_id         bigint, 
    employee_firstname text not null,
    employee_midlname  text not null,
    employee_lastname  text not null
);

create index if not exists employee_department_id_idx on equipment.employes(department_id);