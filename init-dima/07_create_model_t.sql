create table equipment.models(
    id                       bigserial primary key,
    technical_specifications jsonb,
    price                    bigint not null,
    divider                  bigint not null,
    currency_id              bigint not null references equipment.currency(id),
    currency_name            text   not null
);

create index if not exists models_currency_id_id on equipment.models(currency_id);