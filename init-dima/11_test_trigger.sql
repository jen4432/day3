insert into equipment.departments (manager_id, department_name) values (1, 'commercial');

insert into equipment.employes (department_id, manager_id, employee_firstname, employee_midlname, employee_lastname) 
    values (1, null, 'Dmitry', 'Danilovich', 'Beketov');

insert into equipment.warehouse (warehouse_number, responsible_employee_id) values (12345, 1);

insert into equipment.currency (currency_name, updatetime, EUR_ex_rate, USD_ex_rate, BYN_ex_rate, RUB_ex_rate, CNY_ex_rate, divider)
     values ('USD', '2024-01-20', 20, 100, 401, 74, 123, 100);

insert into equipment.models (technical_specifications, price, divider, currency_id, currency_name) 
     values ('{"type":"laptop"}', 10005, 10, 1, 'USD');

insert into equipment.equipments (serial_number, warehouse_id, in_usage, model_id) values (12345, 1, false, 1);

--Данная запись добавится
insert into equipment.employess_equipments (employee_id, equipment_id, start_date, end_date) 
     values (1, 1, '2024-01-28', '2025-01-28');

--На этом запросе выведется уведомления о том что техника с id = 1 сейчас используется
-- insert into equipment.employess_equipments (employee_id, equipment_id, start_date, end_date) 
--      values (2, 1, '2024-01-28', '2025-01-28');
