create table chess.matches(
    id bigserial primary key,
    table_id bigserial not null references chess.tables(id),
    judge_id bigserial not null references chess.judges(id)
);

create index if not exists matches_table_id_idx on chess.matches(table_id);
create index if not exists matches_judge_id_idx on chess.matches(judge_id);