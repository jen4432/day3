create table chess.players(
    id bigserial primary key,
    name text not null,
    rating int
);