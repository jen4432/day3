create table chess.judges(
    id bigserial primary key,
    name text not null
);