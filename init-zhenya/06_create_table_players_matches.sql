create table chess.players_matches(
    player_id bigserial not null references chess.players(id),
    match_id bigserial not null references chess.matches (id),
    primary key (player_id,match_id),
    match_result float not null,
    color text not null check(color = 'White' or color = 'Black')
);

create index if not exists players_matches_match_id_idx on chess.players_matches(match_id);
create index if not exists matches_player_id_idx on chess.players_matches(player_id);

